<!-- Text input-->

<hr>
<div style="border: 2px solid black;">
    <div class="form-group">
    <label class="col-md-12 control-label" for="file_name">File name</label>  
    <div class="col-md-12">
        <input autocomplete="off"id="file_name" name="data[{{ $i }}][file_name]" type="text" placeholder="file name" class="form-control input-md" required="">
        <span class="help-block"></span>  
    </div>
</div>

<!-- Textarea -->
<div class="form-group">
    <label class="col-md-12 control-label" for="code_snippt">Code snippts</label>
    <div class="col-md-12">                     
        <textarea autocomplete="off" class="form-control" id="code_snippt" name="data[{{ $i }}][code]"></textarea>
    </div>
</div>

<!-- Button -->
<div class="form-group">
    <label class="col-md-12 control-label" for="command">command</label>  
    <div class="col-md-12">
        <input autocomplete="off"id="command" name="data[{{ $i }}][command]" type="text" placeholder="command" class="form-control input-md" required="">
        <span class="help-block"></span>  
    </div>
</div>
</div>
<hr>