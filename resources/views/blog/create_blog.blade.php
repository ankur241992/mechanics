@extends('blog.layouts.app')

@section("content")
<div class="row">
    <div class="offset-3 col-md-7">
        <form class="form-horizontal" method="post" action="{{ route('sbmit_blog') }}">
            {{ csrf_field() }}
            <fieldset>

                <!-- Form Name -->
                <legend>Form Name</legend>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-12 control-label" for="title">Title</label>  
                    <div class="col-md-12">
                        <input autocomplete="off"autocomplete="off" id="title" name="topic_name"  type="text" placeholder="title" class="form-control input-md" required="">
                        <span class="help-block"></span>  
                    </div>
                </div>
                
                <div id="grp" >
                    @php 
                    $i =0;
                    @endphp
               @include('blog.ld')
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <button id="singlebutton" name="singlebutton" class="btn btn-primary">Submit</button>&nbsp;&nbsp;
                        <a class="btn btn-primary"  onclick="ld('{{ route('ld') }}')">+</a> 
                    </div>
                </div>

                <!-- Text input-->

            </fieldset>
        </form>

    </div>
</div>
    
@endsection