@extends('blog.layouts.app')

@section("content")
<table class="table table-bordered">
    <thead>
      <tr>
        <th>Topic</th>
        <th>File Name</th>
        <th>Code</th>
      </tr>
    </thead>
    <tbody>
    @foreach($blogs as $blog)	
      <tr>
        <td><span class="text text-success">{{ $blog->topic_name }}</span></td>
        <td><span class="text text-danger">{{ $blog->file_name }}</span></td>
        <td style="background-color: #191616; "><div ><pre><p class="" style="color:#cccc56;"><?php echo htmlentities($blog->code_snippet) ?></p></pre></div></td>
      </tr>
    @endforeach  
    </tbody>
  </table>

@endsection