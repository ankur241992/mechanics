<html>
	<head>
		<link href="{{ asset('/assets/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
	</head>
	
	<body>
		 
		 <nav class="navbar navbar-expand-sm bg-light">
  			<ul class="navbar-nav">
			    
			     <li class="nav-item">
			      	<a class="nav-link" href="{{ route('create_blog') }}">Create blog</a>
			    </li>

			    <li class="nav-item">
			      <a class="nav-link" href="{{ route('read_blog') }}">Read blog</a>
			    </li>
			</ul>
		</nav>


		<div class="container-fluid">
			@yield('content')
		</div>	
	</body>
	<script src="{{ asset('/assets/jquery/dist/jquery.min.js') }}"></script>
	<script src="{{ asset('/assets/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript">
            var i = 1;
            function ld(l){
                
                $.get(l+"?i="+i,function(data){
                    $("#grp").append(data);
                    i++;
                });
                ;
            } 
        </script>
</html>