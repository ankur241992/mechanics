@extends("admin.layouts.authorised")

@section('content')

<div class="row">
  
  <div class="col-md-12">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Create Services</h6>
        </div>
        
        <div class="card-body">
          
          <div class="container-fluid">
            
            <div class="row">

              <div class="col-md-12" style="">
      @if($errors->has('success'))
             <div class="alert alert-success">{{ $errors->first('success') }}</div>
        @elseif($errors->has('error'))
             <div class="alert alert-success">{{ $errors->first('error') }}</div>
        @endif 
        <?php 

                if( isset($service->service_name) ){
                    $service_name = $service->service_name;
                    $action = route('admin.updateservice');
                    $btnText = "Update Service";
                }
                    
                
                else{
                    $service_name = "";
                    $action = route('admin.createservice');
                    $btnText = "Create Service";
                }
            

        ?>
      <form role="form" action="{{ $action }}" method="post">
        

        @if(!empty($service->service_id))
          <input type="hidden" name="service_id"  value="{{ $service->service_id }}">
        @endif
               {{ csrf_field() }}
                          <fieldset>
                            <div class="row">
                              
                              <div class="col-md-10">
                              <div class="form-group">
                                    <label>Service name*</label>
                                    <input class="form-control" value="{{ old('service_name',$service_name) }}" placeholder="enter name" name="service_name" type="text" autofocus>
                                    
                                    @if($errors->has('service_name'))
                                        <p class="text text-danger">{{ $errors->first('service_name') }}</p>
                                    @endif
                                        
                                </div>
              </div>  

                            <div class="col-md-2">
                              <div class="form-group">
                                <label>*</label>
                                <input type="submit" name="submit" value="{{ $btnText }}" class="btn btn-sm btn-success btn-block">
                              </div>
                            </div>

                            </div>
                            
                        </fieldset>
                        </form>

                        <table class="table">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Service Name</th>
                                <th>Action</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach(getServices() as $key => $service)
                                      <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $service->service_name }}</td>
                                        <td> 
                                            <a href="{{ route('admin.editservice',$service->service_id) }}">Edit</a>
                                            
                                        </td>
                                        <td>
                                            &nbsp;<a class="btn btn-info pull-right"  href="{{ route('admin.service_branch',$service->service_id) }}">Add branch</a> 
                                        </td>
                                      </tr>
                                @endforeach          
                            </tbody>

                      </table>
             </div>
      </div>
              
            </div>

          </div>

        </div>
</div>
  </div>





@endsection