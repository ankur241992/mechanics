@extends("admin.layouts.authorised")

@section('content')

<div class="row">
  
  <div class="col-md-12">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Create Services</h6>
        </div>
        
        <div class="card-body">
          
          <div class="container-fluid">
            
            <div class="row">

              <div class="col-md-12" style="">
        @if($errors->has('success'))
             <div class="alert alert-success">{{ $errors->first('success') }}</div>
        @elseif($errors->has('error'))
             <div class="alert alert-success">{{ $errors->first('error') }}</div>
        @endif 
        <?php 

                if( isset($service_branch->sb_name) ){
                    $service_name = $service_branch->sb_name;
                    $action = route('admin.updateservice_branch');
                    $btnText = "Update Service branch";
                    $serviceId = $service_branch->service_id;
                    $serviceName = $service_branch->service_name;
                    $branches = [];
                }
                    
                
                else{
                    $service_name = "";
                    $action = route('admin.createservice_branch');
                    $btnText = "Create Service branch";
                }
            

        ?>
        <form role="form" action="{{ $action }}" method="post">
            

            @if(!empty($service_branch->service_id))
                <input type="hidden" name="sb_id"  value="{{ $service_branch->sb_id }}">
            @endif
               {{ csrf_field() }}
                            <fieldset>

                              <div class="form-group">
                                    <label>Service branch name*</label>
                                    <input class="form-control" value="{{ old('sb_name',$service_name) }}" placeholder="enter name" 
                                    name="sb_name" type="text" autofocus>
                                    
                                    @if($errors->has('sb_name'))
                                        <p class="text text-danger">{{ $errors->first('sb_name') }}</p>
                                    @endif
                                        
                                </div>

                                <div class="form-group">
                                    <label>Service name*</label>
                                    
                                    <select class="form-control" name="service_id">
                                         <option selected="" value="{{ $serviceId }}">{{ $serviceName }}</option>
                                    </select>

                                    
                                    
                                    @if($errors->has('service_name'))
                                        <p class="text text-danger">{{ $errors->first('service_name') }}</p>
                                    @endif
                                        
                                </div>

                                
                                <input type="submit" name="submit" value="{{ $btnText }}" class="btn btn-sm btn-success btn-block">

                            </fieldset>
                        </form>

                        <table class="table">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Service Name</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach($branches as $key => $service)
                                      <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $service->sb_name }}</td>
                                        <td> <a href="{{ route('admin.editservice_branch',$service->sb_id) }}">Edit</a> </td>
                                        <td>
                                            &nbsp;<a class="btn btn-info pull-right"  href="{{ route('admin.service_branch_cat',$service->sb_id) }}">Add service  branch Cat</a> 
                                        </td>
                                      </tr>
                                @endforeach          
                            </tbody>

                      </table>
             </div>
              
            </div>

          </div>

        </div>
</div>
  </div>

</div>



@endsection