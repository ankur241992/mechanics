@extends("admin.layouts.authorised")

@section('content')

<div class="row">
  
  <div class="col-md-12">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Create Service Branch</h6>
        </div>
        
        <div class="card-body">
          
          <div class="container-fluid">
            
            <div class="row">

              <div class="col-md-12" style="">
        @if($errors->has('success'))
             <div class="alert alert-success">{{ $errors->first('success') }}</div>
        @elseif($errors->has('error'))
             <div class="alert alert-error">{{ $errors->first('error') }}</div>
        @endif 
        <?php 

                if( isset($service_branch->branch_cat_name) ){
                    $service_name = $service_branch->branch_cat_name;
                    $action = route('admin.updateservice_branch_cat');
                    $btnText = "Update category";
                }
                    
                
                else{
                    $service_name = "";
                    $action = route('admin.createservice_branch_cat');
                    $btnText = "Create Service branch";
                }
            

        ?>
        <form role="form" action="{{ $action }}" method="post">
            

            @if(!empty($service_branch->sbc_id))
                <input type="hidden" name="sbc_id"  value="{{ $service_branch->sbc_id }}">
            @endif
               {{ csrf_field() }}
                            <fieldset>

                              <div class="form-group">
                                    <label>Service branch name*</label>
                                    <input class="form-control" value="{{ old('branch_cat_name',$service_name) }}" placeholder="enter name" 
                                    name="branch_cat_name" type="text" autofocus>
                                    
                                    @if($errors->has('branch_cat_name'))
                                        <p class="text text-danger">{{ $errors->first('branch_cat_name') }}</p>
                                    @endif
                                        
                                </div>

                                <div class="form-group">
                                    <label>Service name*</label>
                                    
                                    <select class="form-control" name="service_id">
                                       @foreach( getServices() as $s )

                                            @if(isset($service_branch->sb_id) && $service_branch->service_id == $s->service_id)
                                                <option selected="" value="{{ $s->service_id }}">{{ $s->service_name }}</option>

                                            @else
                                                <option value="{{ $s->service_id }}">{{ $s->service_name }}</option>    
                                            @endif
                                            
                                       @endforeach 
                                    </select>

                                </div>

                                <div class="form-group">
                                    <label>Service Branch name*</label>
                                    
                                    <select class="form-control" name="sb_id">
                                       @foreach( getServicesBranch() as $s )

                                            @if(isset($service_branch->sb_id) && $service_branch->sb_id == $s->sb_id)
                                                <option selected="" value="{{ $s->sb_id }}">{{ $s->sb_name }}</option>

                                            @else
                                                <option value="{{ $s->sb_id }}">{{ $s->sb_name }}</option>    
                                            @endif
                                            
                                       @endforeach 
                                    </select>

                                </div>

                                
                                <input type="submit" name="submit" value="{{ $btnText }}" class="btn btn-sm btn-success btn-block">

                            </fieldset>
                        </form>

                        <table class="table">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Service Name</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach(gtServicesBranchCat() as $key => $service)
                                      <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $service->branch_cat_name }}</td>
                                        <td> <a href="{{ route('admin.editservice_branch_cat',$service->sbc_id) }}">Edit</a> </td>
                                      </tr>
                                @endforeach          
                            </tbody>

                      </table>
             </div>
              
            </div>

          </div>

        </div>
</div>
  </div>

</div>



@endsection