@extends ('layouts.plane')
@section ('body')

<div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">

                @if(!empty($success))
                    <div class="alert alert-success">{{ $success }}</div>
                @elseif(!empty($error))
                    <div class="alert alert-success">{{ $error }}</div>
                @endif    
            <br /><br /><br />
               @section ('login_panel_title','Please Sign Up')
               @section ('login_panel_body')
                        <form role="form" action="{{ route('mechanic.registersubmit') }}" method="post">
                            {{ csrf_field() }}
                            <fieldset>

                                <div class="form-group">
                                    <label>Email*</label>
                                    <input class="form-control" value="{{ old('mech_email') }}" placeholder="enter your email id eg : ankur@xyz.com" name="mech_email" type="text" autofocus>
                                    <p class="text text-success">Please Enter email id</p>
                                    @if($errors->has('mech_email'))
                                        <p class="text text-danger">{{ $errors->first('mech_email') }}</p>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Login*</label>
                                    <input class="form-control" value="{{ old('login_id') }}" placeholder="enter 4 digit login id eg : 8767" name="login_id" type="text" autofocus>
                                    <p class="text text-success">Please Enter 4 digit login id eg : 8767</p>
                                    @if($errors->has('mech_login'))
                                        <p class="text text-danger">{{ $errors->first('login_id') }}</p>
                                    @endif
                                </div>
                                
                                <div class="form-group">
                                    <label>Name*</label>
                                    <input class="form-control" value="{{ old('mech_name') }}" placeholder="enter name" name="mech_name" type="text" autofocus>
                                    
                                    @if($errors->has('mech_name'))
                                        <p class="text text-danger">{{ $errors->first('mech_name') }}</p>
                                    @endif
                                        
                                </div>
                                
                                <div class="form-group">
                                    <label>Password*</label>
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                    @if($errors->has('password'))
                                        <p class="text text-danger">{{ $errors->first('password') }}</p>
                                    @endif

                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                
                                <input type="submit" name="submit" value="Register" class="btn btn-lg btn-success btn-block">
                            </fieldset>
                        </form>
                    
                @endsection
                @include('widgets.panel', array('as'=>'login', 'header'=>true))
            </div>
        </div>
    </div>
@stop