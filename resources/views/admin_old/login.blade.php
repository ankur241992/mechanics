@extends("admin.admin_layout")

@section("content")

	<div class="row justify-content-md-center">
		<div class="card">
		  <div class="card-header">
		    Featured
		  </div>
		  <div class="card-body">
		  	@if($errors->has('success'))
                    <div class="alert alert-success">{{ $errors->first('success') }}</div>
                @elseif($errors->has('error'))
                    <div class="alert alert-success">{{ $errors->first('error') }}</div>
                @endif 
		   <form role="form" action="{{ route('admin.loginsbmit') }}" method="post" >
		   	 {{ csrf_field() }}
			  <div class="form-group">
			    <label for="exampleInputEmail1">Email address</label>
			    <input type="email" class="form-control" name="email" value="{{ old('email') }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
			    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
			    @if($errors->has('email'))
                    <p class="text text-danger">{{ $errors->first('email') }}</p>
                @endif
			  </div>
			  <div class="form-group">
			    <label for="exampleInputPassword1">Password</label>
			    <input type="password" name="password" value="{{ old('password') }}" class="form-control" id="exampleInputPassword1" placeholder="Password">
			    @if($errors->has('password'))
                    <p class="text text-danger">{{ $errors->first('password') }}</p>
                @endif
			  </div>
			  <div class="form-group form-check">
			    <input type="checkbox" class="form-check-input" id="exampleCheck1">
			    <label class="form-check-label" for="exampleCheck1">Check me out</label>
			  </div>
			  <button type="submit" class="btn btn-primary">logini</button>
			</form>
		  </div>
		</div>
	</div>

@endsection