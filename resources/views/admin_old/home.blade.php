@extends("admin.authorised")

@section("content")

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12" style="">
    	@if($errors->has('success'))
             <div class="alert alert-success">{{ $errors->first('success') }}</div>
        @elseif($errors->has('error'))
             <div class="alert alert-success">{{ $errors->first('error') }}</div>
        @endif 
        <?php 

                if( isset($service->service_name) ){
                    $service_name = $service->service_name;
                    $action = route('admin.updateservice');
                    $btnText = "Update Service";
                }
                    
                
                else{
                    $service_name = "";
                    $action = route('admin.createservice');
                    $btnText = "Create Service";
                }
            

        ?>
    	<form role="form" action="{{ $action }}" method="post">
    		

    		@if(!empty($service->service_id))
    			<input type="hidden" name="service_id"  value="{{ $service->service_id }}">
    		@endif
               {{ csrf_field() }}
                            <fieldset>

                           	  <div class="form-group">
                                    <label>Service name*</label>
                                    <input class="form-control" value="{{ old('service_name',$service_name) }}" placeholder="enter name" name="service_name" type="text" autofocus>
                                    
                                    @if($errors->has('service_name'))
                                        <p class="text text-danger">{{ $errors->first('service_name') }}</p>
                                    @endif
                                        
                                </div>

                                
                                <input type="submit" name="submit" value="{{ $btnText }}" class="btn btn-sm btn-success btn-block">

                            </fieldset>
                        </form>

                        <table class="table">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Service Name</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach(getServices() as $key => $service)
                                      <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $service->service_name }}</td>
                                        <td> <a href="{{ route('admin.editservice',$service->service_id) }}">Edit</a> </td>
                                      </tr>
                                @endforeach          
                            </tbody>

                      </table>
             </div>
      </div>


     <div class="col-md-12" style="">
        @if($errors->has('success_b'))
             <div class="alert alert-success">{{ $errors->first('success_b') }}</div>
        @elseif($errors->has('error'))
             <div class="alert alert-success">{{ $errors->first('error_b') }}</div>
        @endif 
        <?php 

                if( isset($service_branch->sb_name) ){
                    $service_name = $service->service_name;
                    $action = route('admin.updateservice_branch');
                    $btnText = "Update Service branch";
                }
                    
                
                else{
                    $service_name = "";
                    $action = route('admin.createservice_branch');
                    $btnText = "Create Service branch";
                }
            

        ?>
        <form role="form" action="{{ $action }}" method="post">
            

            @if(!empty($service_branch->service_id))
                <input type="hidden" name="sb_id"  value="{{ $service_branch->sb_id }}">
            @endif
               {{ csrf_field() }}
                            <fieldset>

                              <div class="form-group">
                                    <label>Service branch name*</label>
                                    <input class="form-control" value="{{ old('service_name',$service_name) }}" placeholder="enter name" name="service_name" type="text" autofocus>
                                    
                                    @if($errors->has('service_name'))
                                        <p class="text text-danger">{{ $errors->first('service_name') }}</p>
                                    @endif
                                        
                                </div>

                                <div class="form-group">
                                    <label>Service name*</label>
                                    
                                    <select class="form-control" name="service_id">
                                       @foreach( getServices() as $s )

                                            @if(isset($service_branch->service_id) && $service_branch->service_id == $s->service_id)
                                                <option selected="" value="{{ $s->service_id }}">{{ $s->sb_name }}</option>

                                            @else
                                                <option value="{{ $s->service_id }}">{{ $s->service_name }}</option>    
                                            @endif
                                            
                                       @endforeach 
                                    </select>

                                    
                                    
                                    @if($errors->has('service_name'))
                                        <p class="text text-danger">{{ $errors->first('service_name') }}</p>
                                    @endif
                                        
                                </div>

                                
                                <input type="submit" name="submit" value="{{ $btnText }}" class="btn btn-sm btn-success btn-block">

                            </fieldset>
                        </form>

                        <table class="table">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Service Name</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach(getServicesBranch() as $key => $service)
                                      <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $service->sb_name }}</td>
                                        <td> <a href="{{ route('admin.editservice_branch',$service->sb_id) }}">Edit</a> </td>
                                      </tr>
                                @endforeach          
                            </tbody>

                      </table>
             </div>
      </div>
      

        
</div>

@endsection