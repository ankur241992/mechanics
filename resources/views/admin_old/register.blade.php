@extends("admin.admin_layout")

@section("content")

	<div class="row justify-content-md-center">
		<div class="card">
		  <div class="card-header">
		    register
		  </div>
		  <div class="card-body">
		  		@if($errors->has('success'))
                    <div class="alert alert-success">{{ $errors->first('success') }}</div>
                @elseif($errors->has('error'))
                    <div class="alert alert-success">{{ $errors->first('error') }}</div>
                @endif    
		   <form role="form" action="{{ route('admin.registersbmit') }}" method="post">
                            {{ csrf_field() }}
                            <fieldset>

                           	  <div class="form-group">
                                    <label>Name*</label>
                                    <input class="form-control" value="{{ old('name') }}" placeholder="enter name" name="name" type="text" autofocus>
                                    
                                    @if($errors->has('name'))
                                        <p class="text text-danger">{{ $errors->first('name') }}</p>
                                    @endif
                                        
                                </div>

                                <div class="form-group">
                                    <label>Email*</label>
                                    <input class="form-control" value="{{ old('email') }}" placeholder="enter your email id eg : ankur@xyz.com" name="email" type="text" autofocus>
                                    <p class="text text-success">Please Enter email id</p>
                                    @if($errors->has('mech_email'))
                                        <p class="text text-danger">{{ $errors->first('email') }}</p>
                                    @endif
                                </div>

                                
                                <div class="form-group">
                                    <label>Password*</label>
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                    @if($errors->has('password'))
                                        <p class="text text-danger">{{ $errors->first('password') }}</p>
                                    @endif

                                </div>
                                
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                
                                <input type="submit" name="submit" value="Register" class="btn btn-lg btn-success btn-block">
                            </fieldset>
                        </form>
		  </div>
		</div>
	</div>

@endsection