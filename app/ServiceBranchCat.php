<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceBranchCat extends Model
{
    protected $table = "service_branch_cat";
    
    protected $primaryKey = "sbc_id";

    public function create($data){
    	$this->service_id = $data["service_id"];
    	$this->sb_id 	  = $data["sb_id"];
    	$this->branch_cat_name    = $data["branch_cat_name"];
    	$this->save();
        return 1;
    }

     public function updateServiceBranchCat($data){
     	$service = $this->find($data['sbc_id']);
    	$service->branch_cat_name = $data["branch_cat_name"];
    	$service->service_id      = $data["service_id"];
    	$service->sb_id 	      = $data["sb_id"];
    	$service->save();
    	return 1;
    }

    public function getSingle($id){
    	return $this::where("sbc_id",$id)->first();
    }
    public function getBySbId($id) {
        return $this::where("sb_id",$id)->get();
    }

}
