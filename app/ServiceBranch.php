<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceBranch extends Model
{
    protected $table = "service_branches";
    protected $primaryKey = "sb_id";

    public function create($data){
    	$this->service_id = $data["service_id"];
    	$this->sb_name    = $data["sb_name"];
    	$this->save();
        return 1;
    }

     public function updateServiceBranch($data){
     	$service = $this->find($data['sb_id']);
    	$service->sb_name = $data["sb_name"];
    	$service->service_id = $data["service_id"];
    	$service->save();
    	return 1;
    }

    public function getSingle($id){
    	return $this::where("sb_id",$id)
                ->leftJoin("service","service.service_id","service_branches.service_id")
                ->first();
    }
    
    public function getBySericeId($id) {
        return $this::where("service_id",$id)->get();
    }

}
