<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected  $table = "topic";
    protected $connection = 'blog_connetion';
    public function create($topic){
       $obj = new Topic;
       $obj->topic_name = $topic;
       $obj->save();
       return $obj->id;
    }
    
    
}
