<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;

class Mechanic extends Authenticatable
{
    use Notifiable;

    protected $table = 'mechanics';

    protected $fillable = ['email',  'password'];

    protected $hidden = ['password',  'remember_token'];

    public function register($data){
    	
    	$this->mech_email 	= 	$data["mech_email"];
    	$this->password   	= 	Hash::make($data['password']);
    	$this->login_id	  	= 	$data["login_id"];
    	$this->mech_name  	= 	$data["mech_name"];
    	$this->save();

    	return 1;

    }

}
