<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = "blogs_angular";

    protected $primaryKey = "id";

    protected $connection = 'blog_connetion';

    public function create($data){
       
    	$this->file_name = $data["file_name"];
    	$this->component = "-";
    	$this->code_snippet = $data["code"];
    	$this->command = $data["command"];
    	$this->topic_id = $data["topic_id"];
    	$this->save();
    }  


    public function read(){
    	return $this::get();
    }

}
