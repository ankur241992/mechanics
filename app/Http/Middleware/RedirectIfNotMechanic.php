<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfNotMechanic
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard="mechanic")
    {
        if(!auth()->guard($guard)->check()) {
            return redirect(route('mechanic.login'));
        }
        return $next($request);
    }
}
