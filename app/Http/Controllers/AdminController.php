<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Admin;

use \App\Service;

use \App\ServiceBranch;

use \App\ServiceBranchCat;
	
use Validator;    

use Redirect;

use Auth;

class AdminController extends Controller
{
    
    public function home(){
    	return view("admin.home");
    }

    public function login(){
    	return view("admin.login");
    }

    public function register(){
    	return view("admin.register");
    }

    public function service(){
        return view("admin.service");
    }

    public function service_branch($id, Service $service, ServiceBranch $sb){
        $service = $service->getSingle($id); 
        $serviceId = $id;
        $serviceName = $service->service_name;
        $servieBranches = $sb->getBySericeId($id); 
        return view("admin.service_branch",["serviceId"=>$id,"serviceName"=>$serviceName,"branches"=>$servieBranches]);
    }

    public function service_branch_cat($id, ServiceBranch $service, ServiceBranchCat $sb){
        $service = $service->getSingle($id); 
        $serviceId = $id;
        $serviceName = $service->service_name;
        $servieBranches = $sb->getBySbId($id); 
        return view("admin.service_branch_cat",["sbId"=>$id,"serviceCatName"=>$serviceName,"branches_cat"=>$servieBranches]);
    }

    public function registerSbmit(Request $request,Admin $admin) {
        
       
        $validator = Validator::make($request->all(), [
            'email'=>'required|unique:admin',
            'password'  => 'required',
            'name'=>"required",
        ]);

        if ($validator->fails()) {
            return redirect(route('admin.register'))
                        ->withErrors($validator)
                        ->withInput();
        }

        /*$validator->after(function ($validator) {
            if ($this->somethingElseIsInvalid()) {
                $validator->errors()->add('field', 'Something is wrong with this field!');
            }
        });*/
        if($admin->register($request->all())){
            $validator->errors()->add('success', 'Register successfully !!');
            return Redirect::back()->withErrors($validator);
        }else{
            $validator->errors()->add('error', 'Error in registration !!');
            return Redirect::back()->withErrors($validator);
        }

    }


    public function loginSbmit(Request $request,Admin $admin) {
        
       
        $validator = Validator::make($request->all(), [
            'email'=>'required',
            'password'  => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('admin.login'))
                        ->withErrors($validator)
                        ->withInput();
        }

        /*$validator->after(function ($validator) {
            if ($this->somethingElseIsInvalid()) {
                $validator->errors()->add('field', 'Something is wrong with this field!');
            }
        });*/
        if(Auth::guard('admin')->attempt(["email"=>$request->email,"password"=>$request->password])){
            $validator->errors()->add('success', '');
            return Redirect::back()->withErrors($validator);
        }else{
            $validator->errors()->add('error', 'Email or password is incorrect!!');
            return Redirect::back()->withErrors($validator);
        }

    }

    public function logout(){
         
        Auth::guard("admin")->logout();
        return redirect(route('admin.home'));
             
    }   


     public function createService(Request $request,Admin $admin,Service $service) {
        
       
        $validator = Validator::make($request->all(), [
            'service_name'     =>  'required|unique:service',
        ]);

        if ($validator->fails()) {
            return redirect(route('admin.service'))
                        ->withErrors($validator)
                        ->withInput();
        }

        /*$validator->after(function ($validator) {
            if ($this->somethingElseIsInvalid()) {
                $validator->errors()->add('field', 'Something is wrong with this field!');
            }
        });*/
        if($service->create($request->all())){
            $validator->errors()->add('success', 'Service Created successfully !!');
            return Redirect::back()->withErrors($validator);
        }else{
            $validator->errors()->add('error', 'Error in Service Adding!!');
            return Redirect::back()->withErrors($validator);
        }

    }

    public function editService($serviceId,Request $request,Service $service)  {

        $validator = Validator::make(["service_id"=>$serviceId], [
            'service_id'     =>  'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('404'))
                        ->withErrors($validator)
                        ->withInput();
        }

        return view("admin.service")
                ->with("service",$service->getSingle($serviceId));
    }

    public function updateService(Request $request,Admin $admin,Service $service) {
        
       
        $validator = Validator::make($request->all(), [
            'service_name'     =>  'required|unique:service',
            'service_id'       =>  'required', 
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }

        /*$validator->after(function ($validator) {
            if ($this->somethingElseIsInvalid()) {
                $validator->errors()->add('field', 'Something is wrong with this field!');
            }
        });*/
        if($service->updateService($request->all())){
            $validator->errors()->add('success', 'Service Updated successfully !!');
            return redirect(route('admin.service'))->withErrors($validator);
        }else{
            $validator->errors()->add('error', 'Error in Updating servies!!');
            return Redirect::back()->withErrors($validator);
        }

    }

    public function createServiceBranch(Request $request,Admin $admin,ServiceBranch $service) {
        
        
       
        $validator = Validator::make($request->all(), [
            'sb_name'     =>  'required|unique:service_branches',
            'service_id'       =>  'required'
        ]);

        if ($validator->fails()) {
            return redirect(route('admin.home'))
                        ->withErrors($validator)
                        ->withInput();
        }

        /*$validator->after(function ($validator) {
            if ($this->somethingElseIsInvalid()) {
                $validator->errors()->add('field', 'Something is wrong with this field!');
            }
        });*/
        if($service->create($request->all())){
            $validator->errors()->add('success', 'Service Branch Created successfully !!');
            return Redirect::back()->withErrors($validator);
        }else{
            $validator->errors()->add('error', 'Error in Service Branch Adding!!');
            return Redirect::back()->withErrors($validator);
        }

    }

    public function editServiceBranch($serviceId,Request $request,ServiceBranch $service)  {

        $validator = Validator::make(["sb_id"=>$serviceId], [
            'sb_id'     =>  'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('404'))
                        ->withErrors($validator)
                        ->withInput();
        }

        return view("admin.service_branch")
                ->with("service_branch",$service->getSingle($serviceId));
    }

    public function updateServiceBranch(Request $request,Admin $admin,ServiceBranch $service) {
        
       
        $validator = Validator::make($request->all(), [
            'sb_name'     =>  'required|unique:service_branches',
            'sb_id'            =>  'required', 
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }

        /*$validator->after(function ($validator) {
            if ($this->somethingElseIsInvalid()) {
                $validator->errors()->add('field', 'Something is wrong with this field!');
            }
        });*/
        if($service->updateServiceBranch($request->all())){
            $validator->errors()->add('success', 'Service Updated successfully !!');
            return redirect(route('admin.service_branch',$request->sb_id))->withErrors($validator);
        }else{
            $validator->errors()->add('error', 'Error in Updating servies!!');
            return Redirect::back()->withErrors($validator);
        }

    }

    public function createServiceBranchCat(Request $request,Admin $admin,ServiceBranchCat $service) {
        
       
        $validator = Validator::make($request->all(), [
            'branch_cat_name'     =>  'required|unique:service_branch_cat',
            'service_id'              =>  'required',
            'sb_id'               =>  'required'
        ]);

        if ($validator->fails()) {
            return redirect(route('admin.service_branch_cat'))
                        ->withErrors($validator)
                        ->withInput();
        }

        /*$validator->after(function ($validator) {
            if ($this->somethingElseIsInvalid()) {
                $validator->errors()->add('field', 'Something is wrong with this field!');
            }
        });*/
        if($service->create($request->all())){
            $validator->errors()->add('success', 'Service Branch Category Created successfully !!');
            return Redirect::back()->withErrors($validator);
        }else{
            $validator->errors()->add('error', 'Error in Service Branch Category Adding!!');
            return Redirect::back()->withErrors($validator);
        }

    }

    public function editServiceBranchCat($serviceId,Request $request,ServiceBranchCat $service)  {

        $validator = Validator::make(["sbc_id"=>$serviceId], [
            'sbc_id'     =>  'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('404'))
                        ->withErrors($validator)
                        ->withInput();
        }

        return view("admin.service_branch_cat")
                ->with("service_branch",$service->getSingle($serviceId));
    }

    public function updateServiceBranchCat(Request $request,Admin $admin,ServiceBranchCat $service) {
        
       
        $validator = Validator::make($request->all(), [
            'branch_cat_name'     =>  'required|unique:service_branch_cat',
            'sb_id'       =>  'required', 
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }

        /*$validator->after(function ($validator) {
            if ($this->somethingElseIsInvalid()) {
                $validator->errors()->add('field', 'Something is wrong with this field!');
            }
        });*/
        if($service->updateServiceBranchCat($request->all())){
            $validator->errors()->add('success', 'Service Updated successfully !!');
            return redirect(route('admin.service_branch_cat'))->withErrors($validator);
        }else{
            $validator->errors()->add('error', 'Error in Updating servies!!');
            return Redirect::back()->withErrors($validator);
        }

    }
}
