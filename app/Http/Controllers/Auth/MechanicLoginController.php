<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class MechanicLoginController extends Controller
{

    use AuthenticatesUsers;

    protected $redirectTo = '/mechanic/home';

    /**
     **_ Create a new controller instance.
     _**
     **_ @return void
     _**/
    public function __construct()
    {
      $this->middleware('guest')->except('logout');
    }
    /**
     _
     _ @return property guard use for login
     _
     _/**/
    public function guard()
    {
     return Auth::guard('mechanic');
    }

    // login from for teacher
    public function showLoginForm()
    {
        return view('mechanic.login');
    }

 }

?>
