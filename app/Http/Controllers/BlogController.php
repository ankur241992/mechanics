<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Blog;
use App\Topic;

use Validator;
class BlogController extends Controller
{

	public function index(Request $request){
		return view('blog.create_blog',['edit'=>0]);
	}

    public function create(Request $request,Blog $blog,Topic $topic){

    	//dbg($request->all(),1);
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'topic_name'  	=> 'required',
            'file_name'  	=> 'array',
            'code'   	=> 'array',
            'command'  		=> 'array',
           
            
        ]);

        if ($validator->fails()) {
        	dd($validator->errors());
            /*return redirect()
            			->back()
                        ->withErrors($validator)
                        ->withInput();*/
        }
        
        $topicId = $topic->create($request->topic_name);
        $all = $request->data;
        foreach($all as $data){
             $data['topic_id'] = $topicId; 
             $save = $blog->create($data);
       
        }
        return redirect()->back();

    }

    public function read(Request $request,blog $blog) {
    	$data = $blog->read();
    	return view("blog.read_blog")->with("blogs",$data);
    }
    
    public function ld(Request $r){
        
        return view('blog.ld')->with("i",empty($r->i) ? 0 : $r->i);
    }

}
