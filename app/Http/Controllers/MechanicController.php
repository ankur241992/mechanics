<?php

namespace App\Http\Controllers;

use App\Mechanic;
use Illuminate\Http\Request;
use Validator;

class MechanicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function showRegisterFrom(){
        return view("register_mechanic");
    }
    
    
    
    public function registerMechanic(Request $request,Mechanic $mechanic) {
        
        print "<pre>";
        print_r($request->all());
        print "</pre>";
        //die;

        $validator = Validator::make($request->all(), [
            'login_id'  => 'required|unique:mechanics|max:4',
            'mech_email'=>'required|unique:mechanics',
            'password'  => 'required',
            'mech_name' => 'required',
        ]);

        if ($validator->fails()) {
           // die("in valid");
            return redirect(route('mechanic.register'))
                        ->withErrors($validator)
                        ->withInput();
        }

        /*$validator->after(function ($validator) {
            if ($this->somethingElseIsInvalid()) {
                $validator->errors()->add('field', 'Something is wrong with this field!');
            }
        });*/



        if($mechanic->register($request->all())){
            die("in if");
            return redirect()->back("success","Register successfully!!");
        }else{
            die("in else");
            return redirect()->back("error","Sorry error in registration!!");
        }

    }

    public function codeAlreadyExists(){
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mechanic  $mechanic
     * @return \Illuminate\Http\Response
     */
    public function show(Mechanic $mechanic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mechanic  $mechanic
     * @return \Illuminate\Http\Response
     */
    public function edit(Mechanic $mechanic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mechanic  $mechanic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mechanic $mechanic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mechanic  $mechanic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mechanic $mechanic)
    {
        //
    }
}
