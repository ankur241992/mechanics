<?php

use App\Service;
use App\ServiceBranchCat;
use App\ServiceBranch;

function getServices(){
    return Service::all();
}

function getServicesBranch(){
    return ServiceBranch::all();
}

function gtServicesBranchCat(){
    return ServiceBranchCat::all();
}

function dbg($d,$die=false){
	
		echo "<pre>";
		print_r($d);
		echo "</pre>";
		
		if($die)
			die();
}