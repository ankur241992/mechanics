<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = "service";
    
    protected $primaryKey = "service_id";
    
    public function create($data){
    	$this->service_name = $data["service_name"];
    	$this->save();
    	return 1;
    }

    public function getSingle($id){
    	return $this::where("service_id",$id)->first();
    }

     public function updateService($data){
     	$service = $this->find($data['service_id']);
    	$service->service_name = $data["service_name"];
    	$service->save();
    	return 1	;
    }
}
