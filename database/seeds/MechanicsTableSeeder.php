<?php


use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class MechanicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

	for($i=1;$i<100;$i++){	    
        DB::table('mechanics')->insert([
            'mech_name' => Str::random(6),
	    'login_id' => Str::random(4),
	    'mech_expert_area' => 1,
	    'mech_contact' => Str::random(10),
	    'mech_address' => Str::random(40),
	    'email' => Str::random(10).'@gmail.com',
            'password' => bcrypt(123456),
    	]);
       }	

    }
}
