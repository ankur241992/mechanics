<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMechanicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mechanics', function (Blueprint $table) {
		
		$table->bigIncrements('mechanic_id');
                $table->bigInteger('company_id');
		$table->string('login_id')->unique();
            	$table->string('password');
                $table->char('mech_name', 100);
		$table->bigInteger('mech_expert_area');
		$table->text('mech_address');
		$table->string('mech_contact', 100);
                $table->string('mech_country');
                $table->string('mech_state');
                $table->string('mech_city');
                $table->ipAddress('mech_user_ip');
                $table->boolean('mech_available');
		$table->softDeletes();
		$table->rememberToken();
            	$table->timestamps();
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mechanics');
    }
}
