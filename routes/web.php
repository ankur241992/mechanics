 	<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/




Route::get('/', function()
{
	return View::make('home');
});

Route::get('/charts', function()
{
	return View::make('mcharts');
});

Route::get('/tables', function()
{
	return View::make('table');
});

Route::get('/forms', function()
{
	return View::make('form');
});

Route::get('/grid', function()
{
	return View::make('grid');
});

Route::get('/buttons', function()
{
	return View::make('buttons');
});


Route::get('/icons', function()
{
	return View::make('icons');
});

Route::get('/panels', function()
{
	return View::make('panel');
});

Route::get('/typography', function()
{
	return View::make('typography');
});

Route::get('/notifications', function()
{
	return View::make('notifications');
});

Route::get('/blank', function()
{
	return View::make('blank');
});

Route::get('/login', function()
{
	return View::make('login');
});

Route::get('/documentation', function()
{
	return View::make('documentation');
});





Route::get("blog",'BlogController@index')->name('create_blog');
Route::get("blog/read",'BlogController@read')->name('read_blog');
Route::get("blog/ld",'BlogController@ld')->name('ld');
Route::post("blog/create",'BlogController@create')->name('sbmit_blog');



Route::get('/mechanic/login', 'Auth\MechanicLoginController@showLoginForm')->name('mechanic.login');
Route::post('/mechanic/login', 'Auth\MechanicLoginController@login')->name('mechanic.login.post');
Route::post('/mechanic/logout', 'Auth\MechanicLoginController@logout')->name('mechanic.logout');

Route::get('/mechanic/register', 'MechanicController@showRegisterFrom')->name('mechanic.register');
Route::post('/mechanic/register', 'MechanicController@registerMechanic')->name('mechanic.registersubmit');


Route::group(['middleware'=>'mechanic'], function() {
    Route::get('/mechanic/home', 'MechanicController@index');
});


Route::group(['middleware'=>'admin'], function() {
    Route::get('/admin/home', 'AdminController@home')->name('admin.home');

    Route::get('/admin/service', 'AdminController@service')->name('admin.service');

    Route::get('/admin/service_branch/{id}', 'AdminController@service_branch')->name('admin.service_branch');

    Route::get('/admin/service_branch_cat/{id}', 'AdminController@service_branch_cat')->name('admin.service_branch_cat');

    Route::get('/admin/logout', 'AdminController@logout')->name('admin.logout');
    
    Route::post('/admin/createservice', 'AdminController@createService')
    ->name('admin.createservice');
    Route::post('/admin/updateservice', 'AdminController@updateService')
    ->name('admin.updateservice');
    Route::get('/admin/editservice/{id}', 'AdminController@editService')
    ->name('admin.editservice');

    Route::post('/admin/createservicebranch', 'AdminController@createServiceBranch')
    ->name('admin.createservice_branch');
    Route::post('/admin/updateservicebranch', 'AdminController@updateServiceBranch')
    ->name('admin.updateservice_branch');
    Route::get('/admin/editservicebranch/{id}', 'AdminController@editServiceBranch')
    ->name('admin.editservice_branch');

     Route::post('/admin/createservicebranch_cat', 'AdminController@createServiceBranchCat')
    ->name('admin.createservice_branch_cat');
    Route::post('/admin/updateservicebranch_cat', 'AdminController@updateServiceBranchCat')
    ->name('admin.updateservice_branch_cat');
    Route::get('/admin/editservicebranch_cat/{id}', 'AdminController@editServiceBranchCat')
    ->name('admin.editservice_branch_cat');


});

Route::group(['middleware'=>'isadmin'], function() {
    Route::get('/admin/login', 'AdminController@login')->name('admin.login');
	Route::get('/admin/register', 'AdminController@register')->name('admin.register');
	Route::post('/admin/register', 'AdminController@registerSbmit')->name('admin.registersbmit');
	Route::post('/admin/login', 'AdminController@loginSbmit')->name('admin.loginsbmit');
});
